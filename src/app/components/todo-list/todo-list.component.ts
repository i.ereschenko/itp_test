import {AfterViewInit, Component, OnInit,} from '@angular/core';
import {fromEvent, Observable} from "rxjs";
import {FormBuilder, FormControl} from "@angular/forms";

import {List} from "../../todo.interfaces";

@Component({
    selector: 'app-todo-list',
    templateUrl: './todo-list.component.html',
    styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit, AfterViewInit {

    public lists: List[] = [
        {
            name: 'Homeworks',
            todo: [
                {
                    name: 'apple',
                    done: true
                }
            ],
            countTodoDone: 1
        },
        {
            name: 'Shopping',
            todo: [
                {
                    name: 'apple',
                    done: false
                },
                {
                    name: 'bread',
                    done: true
                },
                {
                    name: 'meat',
                    done: false
                },
                {
                    name: 'milk',
                    done: false
                }
            ],
            countTodoDone: 1
        },
        {
            name: 'Others',
            todo: [
                {
                    name: 'apple',
                    done: false
                },
                {
                    name: 'bread',
                    done: true
                },
                {
                    name: 'meat',
                    done: false
                },
                {
                    name: 'milk',
                    done: false
                },
                {
                    name: 'apple',
                    done: false
                },
                {
                    name: 'bread',
                    done: true
                },
                {
                    name: 'meat',
                    done: false
                },
                {
                    name: 'milk',
                    done: false
                }
            ],
            countTodoDone: 2
        }
    ];
    public newList: FormControl;

    constructor(
        private fb: FormBuilder,
    ) {
        this.newList = this.fb.control([]);
    }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {

    }

    addNewList(): void {
        this.lists.push({
            name: this.newList.value,
            todo: [],
            countTodoDone: 0
        });
        this.newList.setValue('');
    }
}
