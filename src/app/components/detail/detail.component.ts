import {Component, Input, OnInit} from '@angular/core';
import {map, startWith} from "rxjs/operators";
import {FormBuilder, FormControl} from "@angular/forms";
import {Observable} from "rxjs";

import {Detail, List} from "../../todo.interfaces";

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
    @Input() list!: List;
    public filteredTodo?: Observable<Detail[]>;
    public search: FormControl;
    public newTodo: FormControl;
    public showUndone = false;

    constructor(
        private fb: FormBuilder,
    ) {
        this.search = this.fb.control([]);
        this.newTodo = this.fb.control([]);
    }

    ngOnInit() {
        this.setFiltered();
    }

    addNewToDo(ev: any): void {
        this.list.todo.push({
            name: this.newTodo.value,
            done: false
        });
        this.setFiltered();
        this.newTodo.setValue('');
    }

    setFiltered(): void {
        this.filteredTodo = this.search.valueChanges.pipe(
            startWith(''),
            map((value: string) => {
                if (typeof value === 'string') {
                    return this._filter(value);
                }
                return this._filter(' ');
            }),
        );
    }

    private _filter(value: string): Detail[] {
        if (!this.list) {
            return [];
        }
        const filterValue = value.toLowerCase();
        return this.list.todo.filter((todo: Detail) => todo.name.toLowerCase().includes(filterValue));
    }

    deleteTodo(todo: Detail): void {
        console.log(todo);
        const i = this.list.todo.findIndex(value => value === todo);
        console.log(i);
        this.list.todo.splice(i, 1);
        this.setFiltered();
        this.setDoneCount();
    }

    done(todo: Detail): void {
        todo.done = !todo.done;
        this.setDoneCount();
    }

    showUndoneOnly(): void {
        this.showUndone = !this.showUndone;
    }

    setDoneCount(): void {
        const doneList = this.list.todo.filter((todo: Detail) => todo.done === true);
        this.list.countTodoDone = doneList.length;
    }
}
