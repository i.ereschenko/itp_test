export interface Detail {
    name: string,
    done: boolean
}

export interface List {
    name: string,
    todo: Detail[],
    countTodoDone?: number
}